# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=powerdevil
pkgver=5.20.0
pkgrel=0
pkgdesc="Manages the power consumption settings of a Plasma Shell"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="upower"
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev kactivities-dev kauth-dev kidletime-dev kconfig-dev kdbusaddons-dev solid-dev ki18n-dev kglobalaccel-dev kio-dev knotifyconfig-dev kwayland-dev kcrash-dev knotifications-dev libkscreen-dev plasma-workspace-dev bluez-qt-dev networkmanager-qt-dev eudev-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev"
source="https://download.kde.org/stable/plasma/$pkgver/powerdevil-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="09d0893ba0876fc7e1a90cde315cf3aefe3028b99fe3b7edd9fcf7f37b1552175b6514b9a49a68878497215d1ee73938c3efa8b63ddeb4e3be8aeb96b6d640c3  powerdevil-5.20.0.tar.xz"
