# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=systemsettings
pkgver=5.20.0.1
pkgrel=0
pkgdesc="Plasma system manager for hardware, software, and workspaces"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kcrash-dev kitemviews-dev kcmutils-dev ki18n-dev kio-dev kservice-dev kiconthemes-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev kdbusaddons-dev kconfig-dev kdoctools-dev kpackage-dev kdeclarative-dev kactivities-dev kactivities-stats-dev kirigami2-dev plasma-workspace-dev khtml-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/${pkgver%.*}/systemsettings-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="01436f2e290355d83ba32d5f662fe4b39aa8856f6c2c202d84a516cb3cefce0c26a7162def8c70eafeb71d2e446c8e955b3cf42b2149ad653867171121940d16  systemsettings-5.20.0.1.tar.xz"
