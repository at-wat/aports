# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-pa
pkgver=5.20.0
pkgrel=0
pkgdesc="Plasma applet for audio volume management using PulseAudio"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-2.1-only OR LGPL-3.0-only AND GPL-2.0-only"
depends="pulseaudio kirigami2"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev kcoreaddons-dev kdeclarative-dev kdoctools-dev kglobalaccel-dev knotifications-dev ki18n-dev plasma-workspace-dev pulseaudio-dev libcanberra-dev"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_GCONF=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="2440a8d0891d1c2dd3ac0b914f4de9b89a97d87b1ece8f2eaaeb2e28eaa9e0c6015392eed89f290ebc6fcc35805ce7c20d3b87c07dd1c99a186d3c7c1b476160  plasma-pa-5.20.0.tar.xz"
