# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=xdg-desktop-portal-kde
pkgver=5.20.0
pkgrel=0
pkgdesc="A backend implementation for xdg-desktop-portal that is using Qt/KDE"
arch="all !armhf" # armhf blocked by extra-cmake-modules
arch="$arch !mips !mips64 !s390x" # xdg-desktop-portal->flatpak->polkit
url="https://phabricator.kde.org/source/xdg-desktop-portal-kde"
license="LGPL-2.0-or-later"
depends="xdg-desktop-portal"
makedepends="
	cups-dev
	extra-cmake-modules
	glib-dev
	kcoreaddons-dev
	kdeclarative-dev
	kio-dev
	kirigami2-dev
	kwayland-dev
	libepoxy-dev
	pipewire-dev
	plasma-framework-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/xdg-desktop-portal-kde-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="7aeee7085fc194c52c0a1d4b3c5c8569cc29cdda3dde8562e3ba57de1b205da68ce41234afcd916dba070c3d5643001183e2b5c6913b9f775d3a61f7808532e6  xdg-desktop-portal-kde-5.20.0.tar.xz"
