# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kinfocenter
pkgver=5.20.0
pkgrel=0
pkgdesc="A utility that provides information about a computer system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org/workspaces/plasmadesktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only)"
makedepends="extra-cmake-modules qt5-qtbase-dev kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev ki18n-dev kiconthemes-dev kcmutils-dev kio-dev kservice-dev solid-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev kdeclarative-dev kpackage-dev solid-dev kwayland-dev glu-dev"
source="https://download.kde.org/stable/plasma/$pkgver/kinfocenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="8e544db63341cf29e578271b29272d2407c014ddf181ba61f83c6200643e66433e06c067620bd18c9a0a5e337d794800c0e5ca762a2ff733c1de76551fdd5b11  kinfocenter-5.20.0.tar.xz"
